#!/bin/bash

python haskell-pkg-graph.py | tred | dot -Tpdf -Gmclimit=400.0 -Gnslimit=400.0 -Granksep=3 -Grankdir=LR -Gsplines=ortho -Gnodesep=0.1 -Nheight=0.1 -Nshape=box -o haskell-pkg-graph.pdf
