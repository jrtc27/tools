#!/bin/bash
set -e
schroot -c jessie-haskell -- ghc -no-user-package-db -package-db=jessie-sandbox/.cabal-sandbox/x86_64-linux-ghc-7.10.3-packages.conf.d/ --make -O binnmus/binNMUs.hs -o binNMUs
rsync -i binNMUs wuiet:
rsync -i binNMUs paradis:
