#!/usr/bin/python
# encoding:utf8
#
# Copyright (C) 2009, Joachim Breitner <nomeata@debian.org>
#
# Inspired by debian-ocaml-status.py written by Stefano Zacchiroli <zack@debian.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 2 of the License, or (at your option) any later
# version.

import bz2
import datetime
import gzip
import os
import re
import string
import sys
from itertools import *

from debian_bundle import debian_support
from debian_bundle.debian_support import version_compare
from debian_bundle.deb822 import PkgRelation, Deb822
#from genshi.template import TemplateLoader

#arches = ['i386','amd64','powerpc']
arches = 'alpha amd64 armel hppa i386 kfreebsd-amd64 kfreebsd-i386 mips mipsel powerpc s390 sparc'.split()

def patch_pkg_dict(entry):
    if not isinstance(entry, dict): # backward compatibility for debian_support
        entry = dict(map(lambda (x, y): (x.lower(), y), entry))
    return entry


def smart_open(fname):
    """Transparently open a compressed (or not) file."""

    f = None
    if fname.endswith('.gz'):
        f = gzip.GzipFile(fname)
    elif fname.endswith('.bz2'):
        f = bz2.BZ2File(fname)
    else:
        f = open(fname)
    return f

class HaskellLinks:
    def is_interesting_source(self, src):
        return (src['package'] == 'ghc6' or self.is_haskell_lib_src(src))

    def is_haskell_lib_src(self,src):
        if 'build-depends' in  src:
            for rel in PkgRelation.parse_relations(src['build-depends']):
                for opt in rel:
                    if opt['name'] == 'ghc6':
                        return True
        return False

    def main(self):
        # sources will contian haskell libraries + ghc6
        self.sources = {}
        f = smart_open("data/unstable-main-Sources.gz")
        srcfile = debian_support.PackageFile('', file_obj=f)
        try:
            for src in filter(self.is_interesting_source, imap(patch_pkg_dict,srcfile)):
                self.sources[src['package']] = src
        except Exception, e:
            print "E: error while parsing %s, ignoring it." % "data/unstable-main-Sources.gz"
            print "  exception: %s" % e
        f.close()

        print "Found %d haskell library source packages" % len(self.sources)
        print "qa.debian.org:"
        print "http://qa.debian.org/developer.php?packages=" + "+".join(self.sources.keys())
        print "qa.debian.org:"
        print "https://buildd.debian.org/status/package.php?suite=unstable&compact=compact&p=" + "+".join(self.sources.keys())+"&a=" + ",".join(arches)
        

if __name__ == '__main__':
    HaskellLinks().main()

