import Codec.Compression.GZip
import Control.Exception
import Control.Monad
import Data.Maybe
import Data.List
import Data.Char
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Network.Curl
import System.Environment
import System.Exit
import System.Process
import System.Directory
import System.IO
import System.IO.Error
import Text.Regex.PCRE
import qualified Data.ByteString.Lazy.Char8 as B
import qualified System.IO.Strict as S

-- Constants

fileSuffix :: String
fileSuffix = "debian" -- Hardcoded for now, because there are no supported
                      -- derivatives.

outputURL :: String
outputURL = "https://release.debian.org/britney/"

packageNotFoundMsg :: String
packageNotFoundMsg
    = "The package you requested was not processed by the autohinter.\n\
       \grep-excuses <pkg> should list all reasons why this package doesn't \
       \migrate."

-- Main

main = withCurlDo $ do
         (package, showAging) <- getArgs >>= parse
         output <- fmap lines
                    $ acquireFile outputURL "update_output.txt" fileSuffix False
         let bins = getBinBlockers output package
         result <- try (fmap nub $ mapM getSrcPackage bins) :: IO (Either ErrorCall [String])
         srcBlockers <- case result of
                          Left e -> putStrLn packageNotFoundMsg >> exitFailure
                          Right pkgs -> return pkgs
         excuses <- mapM getExcuse srcBlockers
         additionalExcuses <- getAdditionalExcuses fileSuffix srcBlockers excuses
         filteredExcuses <- filterExcuses
                            (isInteresting fileSuffix showAging)
                            $ excuses ++ additionalExcuses
         mapM_ putStrLn $ map excuses2String filteredExcuses

-- Command line parsing

parse :: [String] -> IO (String, Bool)
parse [package] = return (package, False)
parse ["--show-aging", package] = return (package, True)
parse [package, "--show-aging"] = return (package, True)
parse _ = printUsage >> exitFailure

printUsage :: IO ()
printUsage = do
  progName <- getProgName
  putStrLn $ "Usage: " ++ progName ++ " [--show-aging] package-name"

-- Utility

maybeTail :: [a] -> Maybe [a]
maybeTail [] = Nothing
maybeTail (x:xs) = Just xs

removeFieldPrefix :: String -> String
removeFieldPrefix arch = drop 2 $ dropWhile (/= ':') arch

matches :: String -> String -> Bool
str `matches` pattern = (not . null) (str =~ pattern :: String)

-- Excuses

data Excuses = Excuses String [String]

isEmpty :: Excuses -> Bool
isEmpty (Excuses _ []) = False
isEmpty (Excuses _ _) = True

excuses2String :: Excuses -> String
excuses2String (Excuses pkg excuses) = unlines
                                       $ (pkg ++ ":"):(map ("    " ++) excuses)

flattenExcuses :: [Excuses] -> [String]
flattenExcuses excuses = concat $ map unpackExcuses excuses

unpackExcuses :: Excuses -> [String]
unpackExcuses (Excuses _ excuses) = excuses

-- File acquirance

acquireFile :: String -> String -> String -> Bool -> IO String
acquireFile urldir filename suffix ungz = do
      cachePath <- chooseCachePath
      case cachePath of 
        Nothing -> do
                   (status, text) <- curlGetString url [CurlFollowLocation True]
                   check status
                   return text
        Just path -> do
                   createDirectoryIfMissing False path
                   setCurrentDirectory path
                   time <- getTime
                   readProcess "/usr/bin/curl" [ "-R"
                                               , "-s"
                                               , "-o", savename
                                               , "-z", savename
                                               , url] ""
                   file <- openFile savename ReadMode
                   if ungz
                   then hSetBinaryMode file True
                   else hSetEncoding file utf8
                   text <- S.hGetContents file
                   hClose file
                   return $ if ungz
                            then B.unpack $ decompress $ B.pack text
                            else text
    where url = urldir ++ ('/':filename)
          getTime = do
            result <- tryJust (\e -> if isDoesNotExistError e
                                     then Just e
                                     else Nothing)
                      $ fmap (floor . utcTimeToPOSIXSeconds)
                            $ getModificationTime $ filename ++
                                  ('.':suffix)
            return $ case result of
                       Right time -> time
                       Left _ -> 0
          check status = if status /= CurlOK
                         then putStrLn ("Download of " ++ url ++ " failed (" ++
                                       show status ++ ")")
                                  >> exitFailure
                         else return ()
          savename = filename ++ ('.':suffix)

chooseCachePath :: IO (Maybe String)
chooseCachePath = do
  result <- tryJust shouldCatch $ getAppUserDataDirectory "reasons"
  hasHome <- getHomeDirectory >>= doesDirectoryExist
  return $ case result of
             Right dir -> if hasHome
                          then Just dir
                          else Nothing
             Left _ -> Nothing
      where shouldCatch e = if isDoesNotExistError e
                            then Just e
                            else Nothing

-- Excuse filtering

filterExcuses :: (String -> String -> IO Bool) -> [Excuses] -> IO [Excuses]
filterExcuses f excuses = fmap (filter isEmpty) $ mapM filterPkgExcuses excuses
    where filterPkgExcuses (Excuses pkg excuses)
              = fmap (Excuses pkg) $ filterM (f pkg) excuses

isInteresting :: String -> Bool -> String -> String -> IO Bool
isInteresting fileSuffix showAging pkg excuse = do
  interestingOUD <- isInterestingOUD showAging fileSuffix pkg excuse
  return $ interestingOUD
             || "introduces new bugs" `isInfixOf` excuse
             || ("Too young" `isPrefixOf` excuse && showAging)
             || "unsatisfiable Depends" `isInfixOf` excuse

isInterestingDependency :: [String] -> String -> String -> IO Bool
isInterestingDependency pkgs _ excuse = return
                                        $ "(not considered)" `isSuffixOf` excuse
                                        && (mangleDependency excuse) `notElem` pkgs

isInterestingOUD :: Bool -> String -> String -> String -> IO Bool
isInterestingOUD True _ _ excuse = return $ "out of date" `isPrefixOf` excuse
isInterestingOUD False fileSuffix pkg excuse
    = if "out of date" `isPrefixOf` excuse
      then do
        let arch = mangleOUD excuse
        text <- acquireFile
                "https://buildd.debian.org/stats/"
                (arch ++ "-dump.txt.gz") 
                fileSuffix True
        return $ (status $ B.pack text) /= (B.pack "BD-Uninstallable")
      else return False
    where status = builddField pkg "state"
 

builddField :: String -> String -> B.ByteString -> B.ByteString
builddField pkg field = (=~ (B.pack $ "(?<=" ++ field ++ ":  ).*(?=\n(.+\n)*package:  "
                             ++ pkg ++ ")"))

mangleDependency :: String -> String
mangleDependency excuse
    | null dependency = ""
    | otherwise = tail $ dropWhile (/= ' ') dependency
    where dependency = excuse =~ "(?<=Depends: ).*(?= \\(not considered\\))"

mangleOUD :: String -> String
mangleOUD = (=~ "(?<=out of date on ).*(?=:)")

-- Fetching Excuses

-- Takes a list of already fetched excuses and returns the excuses of missing dependencies
getAdditionalExcuses :: String -> [String] -> [Excuses] -> IO [Excuses]
getAdditionalExcuses _ _ [] = return []
getAdditionalExcuses fileSuffix pkgs excuses = do
  interestingDepends <- filterExcuses
                        (isInterestingDependency pkgs)
                        excuses
  let dependencies = nub $ map mangleDependency $ flattenExcuses interestingDepends
  excuses <- mapM getExcuse dependencies
  evenMoreExcuses <- getAdditionalExcuses fileSuffix (pkgs ++ dependencies) excuses
  return $ excuses ++ evenMoreExcuses

getExcuse :: String -> IO Excuses
getExcuse pkg = do
  --hPutStrLn stderr $ "retrievieng excuses for " ++ pkg
  excuses <- readProcess "/usr/bin/grep-excuses" [pkg] ""
  return $ Excuses pkg $ map (dropWhile isSpace)
             $ fromMaybe [] $ maybeTail $ lines excuses

-- Package Gathering

getSrcPackage :: String -> IO String
getSrcPackage bin = do
  --hPutStrLn stderr $ "querying source for " ++ bin
  packageDesc <- readProcess "/usr/bin/apt-cache" ["showsrc", bin] ""
  return $ parseDesc packageDesc

getBinBlockers :: [String] -> String -> [String]
getBinBlockers output package = let arches = getArches package output
                                in nub $ map stripComma
                                           $ concat
                                           $ map (words . removeFieldPrefix) arches
                                    where stripComma str = if last str == ','
                                                           then init str
                                                           else str

parseDesc :: String -> String
parseDesc desc = let ls = lines desc
                     srcln = findSourceLine ls
                 in removeFieldPrefix srcln

findSourceLine :: [String] -> String
findSourceLine (curLine:rest)
    | "Package: " `isPrefixOf` curLine = curLine
    | otherwise = findSourceLine rest

-- Britney output parsing

getArches :: String -> [String] -> [String]
getArches package output = get $ removeStats $ fromJust $ findAutohint package output
    where get (line:rest)
              | line `matches` " *\\* .*:" = line : get rest
              | otherwise = []

findAutohint :: String -> [String] -> Maybe [String]
findAutohint _ [] = Nothing
findAutohint package (curLine:rest)
    | curLine `matches` ("Trying easy from autohinter.*" ++ package)
        = Just rest
    | otherwise = findAutohint package rest

removeStats :: [String] -> [String]
removeStats = drop 4
